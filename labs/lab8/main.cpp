#include <iostream>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

#include <cmath>

const int infinity = INT_MAX;

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {

        }
};

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

float sphereIntersection(Sphere s, Ray& r){
    glm::vec3 oc = r.origin - s.center;
    float a = glm::dot(r.direction, r.direction);
    float b = 2.0 * glm::dot(oc, r.direction);
    float c = glm::dot(oc,oc) - s.radius * s.radius;
    float discriminant = b * b - 4 * a * c;
    if(discriminant < 0){
        return infinity;
    }
    else if(-b - sqrt(discriminant) > 0){
        return (-b - sqrt(discriminant)) / (2.0 * a);
    }
    else if(-b + sqrt(discriminant) > 0){
        return (-b + sqrt(discriminant)) / (2.0 * a);
    }
    else{
        return infinity;
    }
}


void renderOrtho(bitmap_image& image, const std::vector<Sphere>& world) {
    // TODO: implement ray tracer
    Viewport view = Viewport(glm::vec2(-5,5),glm::vec2(5,-5));

    float r = view.max.x;
    float l = view.min.x;
    float t = view.max.y;
    float b = view.min.y;
    float nx = image.width();
    float ny = image.height();

    for(int i = 0; i <= image.width(); i++){
      for(int j = 0; j <= image.height(); j++){

        float ui = l + (r-l)*(i+.5) / nx;
        float vj = b + (t-b)*(j+.5) / ny;
        Ray rij;
        rij.origin = glm::vec3(ui, vj, 0);
        rij.direction = glm::vec3(0, 0, 1);


        float closestD = infinity;
        glm::vec3 color;


        for(Sphere s : world){
            float d = sphereIntersection(s, rij);

            if (d < closestD){
                closestD = d;
                color = s.color;

            }
        }
        if (closestD < infinity){
            image.set_pixel(i, j, make_colour(color.x * 255, color.y * 255, color.z * 255));
        }
        else{
            rgb_t colour = make_colour(52, 255, 28);
            image.set_pixel(i,j,colour);
        }

      } 
    }
    image.save_image("orthographic.bmp");
}

void renderPers(bitmap_image& image, const std::vector<Sphere>& world) {
    // TODO: implement ray tracer
    Viewport view = Viewport(glm::vec2(-5,5),glm::vec2(5,-5));

    float r = view.max.x;
    float l = view.min.x;
    float t = view.max.y;
    float b = view.min.y;
    float nx = image.width();
    float ny = image.height();

    for(int i = 0; i <= image.width(); i++){
      for(int j = 0; j <= image.height(); j++){

        float ui = l + (r-l)*(i+.5) / nx;
        float vj = b + (t-b)*(j+.5) / ny;
        Ray rij;
        rij.origin = glm::vec3(0, 0, -5);
        rij.direction = normalize(glm::vec3(ui, vj, 0) - (rij.origin));


        float closestD = infinity;
        glm::vec3 color;


        for(Sphere s : world){
            float d = sphereIntersection(s, rij);

            if (d < closestD){
                closestD = d;
                color = s.color;

            }
        }
        if (closestD < infinity){
            image.set_pixel(i, j, make_colour(color.x * 255, color.y * 255, color.z * 255));
        }
        else{
            rgb_t colour = make_colour(52, 255, 28);
            image.set_pixel(i,j,colour);
        }

      } 
    }
    image.save_image("perspective.bmp");
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 10), 5, glm::vec3(1,1,1)),
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
        Sphere(glm::vec3(4, 4, 6), 1, glm::vec3(.796,.376,1)),
        Sphere(glm::vec3(-4, -4, 6), 1, glm::vec3(.796,.376,1)),
        Sphere(glm::vec3(4, -4, 6), 1, glm::vec3(.796,.376,1)),
    };

    // render the world
    renderOrtho(image, world);
    renderPers(image, world);

    
    std::cout << "Success" << std::endl;
}


