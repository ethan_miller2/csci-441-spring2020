#version 330 core

out vec4 fragColor;
in vec2 uv;

uniform sampler2D ourTexture;

void main() {
	vec4 color = texture(ourTexture, uv);
    fragColor = color;
}
