#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#define _USE_MATH_DEFINES // for C++
#include <cmath>

using namespace std;
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "matrix4.h"

float * ptr;
int mode = 0;

float * rotate(float degrees){
    static float a[16];

    Matrix4* matrixPointer = new Matrix4();

    matrixPointer->initToRotation(degrees);


    return matrixPointer->getValues();
 }


float* staticImage(){
    static float a[16];

    Matrix4* matrixPointer = new Matrix4();

    matrixPointer->initToZero();
    matrixPointer->initToIdentity();


    return matrixPointer->getValues();

 }

float* scale(float x, float y){
    static float a[16];

    Matrix4* matrixPointer = new Matrix4();

    matrixPointer->initToScale(x,y);


    return matrixPointer->getValues();

 }

 float* translate(float tx, float ty){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToTranslation(tx,ty);

    cout << matrixPointer->to_string();

    return matrixPointer->getValues();
 }

 float* rotateOffCenter(float degrees, float x, float y){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToRotationOffCenter(degrees, x, y);

    return matrixPointer->getValues();

 }


float* fun(float scaleX, float scaleY, float degrees, float transX, float transY){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer-> initToFun(scaleX, scaleY, degrees, transX, transY);
    return matrixPointer->getValues();

}



void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {

    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
 }

 void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
        mode++;
}

int main(void) {

    ptr = staticImage();

    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }
    
#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */
        unsigned int transformLoc = glGetUniformLocation(shader.id(), "transform");
        glUniformMatrix4fv(transformLoc, 1, GL_TRUE, ptr);

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
      
        glfwPollEvents();


        if(mode % 5 == 0){
            ptr = staticImage();
        }
        if(mode % 5 == 1){
            ptr = scale(.5 * abs(cos(glfwGetTime())),.5 * abs(cos(glfwGetTime())));
        }
        if(mode % 5 == 2){

            ptr = rotate(20 * glfwGetTime());
        }
        if(mode % 5 == 3){
            // ptr = translate(.2,.4);
            // ptr = rotate(20 * glfwGetTime());
            // ptr = translate(-.2,-.4);
            ptr = rotateOffCenter(20 * glfwGetTime(), .2, .4);

        }
        if(mode % 5 == 4){
            ptr = fun(.2,.3,400 * glfwGetTime(), -cos(glfwGetTime()),-cos(glfwGetTime()));

        }
    
    }

    glfwTerminate();
    return 0;
}
