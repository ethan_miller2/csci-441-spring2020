#ifndef _CSCI441_MATRIX3_H_
#define _CSCI441_MATRIX3_H_

#include <string>
#include <sstream>
#define _USE_MATH_DEFINES // for C++
#include <cmath>

#define _USE_MATH_DEFINES // for C
#include <math.h>
using namespace std;

class Matrix4 {
	float values[16];
	
	
public:
	

	Matrix4() {
	}

	void initToIdentity(){
		for(int i = 0; i < 16; i++){
			if(i % 5 == 0){
				values[i] = 1;
			}
			else{
				values[i] = 0;
			}
		}
	}

	void initToZero(){
		for(int i = 0; i < 16; i++){
			values[i] = 0;
		}
	}

	void initToScale(float x, float y){

		initToZero();
		initToIdentity();


		values[0] = x;
		values[5] = y;

	}

	void initToRotationOffCenter(float degrees, float tx, float ty){

		float theta = degrees * M_PI/180;

		initToZero();
		initToIdentity();
		initToRotation(degrees);
		values[3] = -tx * cos(theta) + ty * sin(theta) + tx;
		values[7] = -tx * sin(theta) - ty * cos(theta) + ty;
	

	}

	void initToFun(float scaleX, float scaleY, float degrees, float transX, float transY){
		initToZero();
		initToIdentity();

		float theta = degrees * M_PI/180;
		values[0] = scaleX * cos(theta);
		values[1] = sin(theta);
		values[4] = -sin(theta);
		values[5] = scaleY * cos(theta);
		values[3] = transX;
		values[7] = transY;
	}


	void initToRotation(float x){

		float theta = x * M_PI/180;

		initToZero();
		initToIdentity();
		values[0] = cos(theta);
		values[1] = -sin(theta);
		values[4] = sin(theta);
		values[5] = cos(theta);
	 
	}
	void initToTranslation(float tx, float ty){
		initToZero();
		initToIdentity();
		values[3] = tx;
		values[7] = ty;
		values[11] = 1;
		// values[11] = ;

	}
	
	float* getValues(){
		return values;
	}


	string to_string() const{
		ostringstream os;
		for(int i = 0; i < 4; i++){
			os << values[i] << " ";

		}
		os << endl;
		for(int i = 4; i < 8; i++){
			os << values[i] << " ";

		}
		os << endl;
		for(int i = 8; i < 12; i++){
			os << values[i] << " ";

		}
		os << endl;
		for(int i = 12; i < 16; i++){
			os << values[i] << " ";

		}
		os << endl;

		cout << endl;
		

		return os.str();	
	}


};


#endif