#include <iostream>
using namespace std;
#include "bitmap_image.hpp"

//Coordinate class to hold x,y values and r,g,b values
class Coord {
public:
  float x;
  float y;

  float r;
  float g;
  float b;



  Coord(){
    x=0;
    y=0;

    r=0;
    g=0;
    b=0;
  }

  // Constructor
  Coord(float xx, float yy, float rr, float gg, float bb) : x(xx), y(yy), r(rr), g(gg), b(bb) {

  }

  

  // Destructor - called when an object goes out of scope or is destroyed
  ~Coord() {
    // this is where you would release resources such as memory or file descriptors
    // in this case we don't need to do anything
  }
};

//Function to get coordinates from the user
Coord * getUserInput(){
    static Coord pointlist[3];
    char comma;
    char colon;

    cout << "Enter 3 points (enter a point as x,y:r,g,b):" <<  endl;

    for(int i = 0; i < 3; i++)
    {
      cin >> pointlist[i].x >> comma >> pointlist[i].y >> colon >> pointlist[i].r >> comma >> pointlist[i].g >> comma >> pointlist[i].b;
    }


  
    return pointlist;
}

//Function to calculate area of a triangle given 3 points
float getTriangleArea(Coord a[]){

  float part1;
  float part2;
  float part3;

  float area;

  part1 = a[0].x * (a[1].y - a[2].y);

  part2 = a[1].x * (a[2].y - a[0].y);

  part3 = a[2].x * (a[0].y - a[1].y);

  area = (part1 + part2 + part3) / 2;

  return area;
}


//Function to determine barycentric values given a Point, an array of three points, and an area of a triangle produced by those three points
float* calcBarycentric(float a, float x, float y, Coord array[]){
  Coord betaTri[3]; 
  Coord gammaTri[3];

  static float barycentricCoords[3];

  betaTri[0].x = array[0].x;
  betaTri[0].y = array[0].y;

  betaTri[1].x = x;
  betaTri[1].y = y;

  betaTri[2].x = array[2].x;
  betaTri[2].y = array[2].y;

  float betaArea = getTriangleArea(betaTri);

  float beta = betaArea/a;


  gammaTri[0].x = array[0].x;
  gammaTri[0].y = array[0].y;

  gammaTri[1].x = array[1].x;
  gammaTri[1].y = array[1].y;

  gammaTri[2].x = x;
  gammaTri[2].y = y;

  float gammaArea = getTriangleArea(gammaTri);
  float gamma = gammaArea/a;

  float alpha = 1 - beta - gamma;

  barycentricCoords[0] = alpha;
  barycentricCoords[1] = beta;
  barycentricCoords[2] = gamma;

  return barycentricCoords;
}

int main(int argc, char** argv) {

    float maxX;
    float minX;

    float minY;
    float maxY;

    Coord* ptrPoints = getUserInput();



    cout << "You Entered: " <<  endl;

    for(int i = 0; i < 3; i++)
    {
      cout << ptrPoints[i].x << ", " << ptrPoints[i].y << " : " <<  ptrPoints[i].r << ", " << ptrPoints[i].g << ", " << ptrPoints[i].b <<endl;
    } 

   
  

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    //Calculate maximum and minimum of all points for bounding box.
    maxX = ptrPoints[0].x;
    minX = ptrPoints[0].x;

    maxY = ptrPoints[0].y;
    minY = ptrPoints[0].y;


    for(int i = 1; i < 2; i++)
    {
      if(ptrPoints[i].x > maxX){
        maxX = ptrPoints[i].x;
      }
    }

    for(int i = 1; i < 3; i++)
    {
      if(ptrPoints[i].x < minX){
        minX = ptrPoints[i].x;
      }
    }

    for(int i = 1; i < 3; i++)
    {
      if(ptrPoints[i].y > maxY){
        maxY = ptrPoints[i].y;
      }
    }
    for(int i = 1; i < 3; i++)
    {
      if(ptrPoints[i].y < minY){
        minY = ptrPoints[i].y;
      }
    }

    // cout << "Min X: " << minX <<  endl;

    // cout << "Max X: " << maxX <<  endl;

    // cout << "Min Y: " << minY <<  endl;

    // cout << "Max Y: " << maxY <<  endl;

    float triArea = getTriangleArea(ptrPoints);
    // cout << "Area: " << triArea <<  endl;

    for(int x = minX; x <= maxX; x++){
      for(int y = minY; y <= maxY; y++){
        float * ptrBarycentric = calcBarycentric(triArea, x, y, ptrPoints);
        if((ptrBarycentric[0] > 0 && ptrBarycentric[0] < 1) && (ptrBarycentric[1] > 0 && ptrBarycentric[1] < 1) && (ptrBarycentric[2] > 0 && ptrBarycentric[2] < 1) ){
          // cout << ptrBarycentric[0] << ", " << ptrBarycentric[1] << " : " <<  ptrBarycentric[2] <<endl;
          rgb_t colour = make_colour(((ptrBarycentric[0] * ptrPoints[0].r) + (ptrBarycentric[1] * ptrPoints[1].r) + (ptrBarycentric[2] * ptrPoints[2].r)) * 255, ((ptrBarycentric[0] * ptrPoints[0].g) + (ptrBarycentric[1] * ptrPoints[1].g) + (ptrBarycentric[2] * ptrPoints[2].g)) * 255, ((ptrBarycentric[0] * ptrPoints[0].b) + (ptrBarycentric[1] * ptrPoints[1].b) + (ptrBarycentric[2] * ptrPoints[2].b)) * 255);
          image.set_pixel(x,y,colour);
        }
      }    
    }

    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
    
}


