#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;


uniform mat3 normModel;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;

out vec3 Normal;
out vec3 FragPos;
out vec3 ourColor;


void main() {

    gl_Position = projection * camera * model * vec4(aPos, 1.0);
    FragPos = vec3(model * vec4(aPos, 1.0));
    Normal = normModel * aNormal;
    ourColor = aColor; 
}
