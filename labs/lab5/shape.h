#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>


template <typename T, typename P, typename N, typename C>
void add_vertex(T& coords, const P& x, const P& y, const P& z, const N& nx, const N& ny, const N& nz,
        const C& r, const C& g, const C& b, bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);
    coords.push_back(nx);
    coords.push_back(ny);
    coords.push_back(nz);
    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);
}

template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
        const C& r, const C& g, const C& b, bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);
    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);
}


class DiscoCube {
public:
    std::vector<float> coords;
    DiscoCube() : coords{
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f, 
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f, 0.0f, 0.0f,   1.0f, 1.0f, 1.0f,  
        -0.5f,  0.5f, -0.5f, -1.0f, 0.0f, 0.0f,   1.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f,   1.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f,   1.0f, 1.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, -1.0f, 0.0f, 0.0f,   1.0f, 1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f, -1.0f, 0.0f, 0.0f,   1.0f, 1.0f, 1.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 1.0f
    } {}

};

class Cylinder {
public:
    std::vector<float> coords;
    Cylinder(unsigned int n, float r, float g, float b) {
        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            Vector4 a1(vi_x, vi_y, -h);
            Vector4 b1(vi_x, vi_y, h);
            Vector4 c1(vip1_x, vip1_y, -h);

            Vector4 w((b1 - a1).cross(c1 - a1));
            float nX1 = -w.x();
            float nY1 = -w.y();
            float nZ1 = -w.z();


            // add triangle viL, viH, vip1L
            add_vertex(coords, vi_x, vi_y, -h, nX1, nY1, nZ1, r, g, b);
            add_vertex(coords, vi_x, vi_y, h, nX1, nY1, nZ1, r, g, b);
            add_vertex(coords, vip1_x, vip1_y, -h, nX1, nY1, nZ1, r, g, b);


            Vector4 a2(vip1_x, vip1_y, -h);
            Vector4 b2(vi_x, vi_y, h);
            Vector4 c2(vip1_x, vip1_y, h);

            Vector4 w2((b2 - a2).cross(c2 - a2));
            float nX2 = -w2.x();
            float nY2 = -w2.y();
            float nZ2 = -w2.z();




            // add triangle vip1L, viH, vip1H
            add_vertex(coords, vip1_x, vip1_y, -h, nX2, nY2, nZ2, r, g, b);
            add_vertex(coords, vi_x, vi_y, h, nX2, nY2, nZ2, r, g, b);
            add_vertex(coords, vip1_x, vip1_y, h, nX2, nY2, nZ2, r, g, b);

            Vector4 a3(vip1_x, vip1_y, h);
            Vector4 b3(vi_x, vi_y, h);
            Vector4 c3(c_x, c_y, h);

            Vector4 w3((b3 - a3).cross(c3 - a3));
            float nX3 = -w3.x();
            float nY3 = -w3.y();
            float nZ3 = -w3.z();

            // add high triangle vi, vip1, 0
            add_vertex(coords, vip1_x, vip1_y, h, nX3, nY3, nZ3, r, g, b);
            add_vertex(coords, vi_x, vi_y, h, nX3, nY3, nZ3, r, g, b);
            add_vertex(coords, c_x, c_y, h, nX3, nY3, nZ3, r, g, b);


            Vector4 a4(vip1_x, vip1_y, -h);
            Vector4 b4(c_x, c_y, -h);
            Vector4 c4(vi_x, vi_y, -h);

            Vector4 w4((b4 - a4).cross(c4 - a4));
            float nX4 = -w4.x();
            float nY4 = -w4.y();
            float nZ4 = -w4.z();

            // // add low triangle vi, vip1, 0
            add_vertex(coords, vip1_x, vip1_y, -h, nX4, nY4, nZ4, r, g, b);
            add_vertex(coords, c_x, c_y, -h,  nX4, nY4, nZ4, r, g, b);
            add_vertex(coords, vi_x, vi_y, -h,  nX4, nY4, nZ4, r, g, b);
        }
    }
};


class Cone {
public:
    std::vector<float> coords;
    Cone(unsigned int n, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            Vector4 a1(vi_x, vi_y, -h);
            Vector4 b1(c_x, c_y, h);
            Vector4 c1(vip1_x, vip1_y, -h);

            Vector4 w((b1 - a1).cross(c1 - a1));
            float nX1 = -w.x();
            float nY1 = -w.y();
            float nZ1 = -w.z();



            // add triangle viL, viH, vip1L
            add_vertex(coords, vi_x, vi_y, -h, nX1, nY1, nZ1, r, g, b);
            add_vertex(coords, c_x, c_y, h, nX1, nY1, nZ1, r, g, b);
            add_vertex(coords, vip1_x, vip1_y, -h, nX1, nY1, nZ1, r, g, b);

            // // add low triangle vi, vip1, 0

            Vector4 a2(vip1_x, vip1_y, -h);
            Vector4 b2(c_x, c_y, -h);
            Vector4 c2(vi_x, vi_y, -h);

            Vector4 w2((b2 - a2).cross(c2 - a2));
            float nX2 = -w2.x();
            float nY2 = -w2.y();
            float nZ2 = -w2.z();
            add_vertex(coords, vip1_x, vip1_y, -h, nX2, nY2, nZ2, r, g, b);
            add_vertex(coords, c_x, c_y, -h, nX2, nY2, nZ2, r, g, b);
            add_vertex(coords, vi_x, vi_y, -h, nX2, nY2, nZ2, r, g, b);
        }
    }
};

class Sphere {
    double x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    double y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

    double z(float r, float phi, float theta){
        return r*cos(phi);
    }

public:
    std::vector<float> coords;
    Sphere(unsigned int n, float radius, float r, float g, float b) {
        int n_steps = (n%2==0) ? n : n+1;
        double step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps; ++i) {
            for (int j = 0; j < n_steps/2.0; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n_steps)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                double vij_x = x(radius, phi_i, theta_j);
                double vij_y = y(radius, phi_i, theta_j);
                double vij_z = z(radius, phi_i, theta_j);


                // vertex i+1,j
                double vip1j_x = x(radius, phi_ip1, theta_j);
                double vip1j_y = y(radius, phi_ip1, theta_j);
                double vip1j_z = z(radius, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(radius, phi_i, theta_jp1);
                double vijp1_y = y(radius, phi_i, theta_jp1);
                double vijp1_z = z(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                double vip1jp1_y = y(radius, phi_ip1, theta_jp1);
                double vip1jp1_z = z(radius, phi_ip1, theta_jp1);


                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, vijp1_x, vijp1_y, vijp1_z, r, g, b);


                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }
};

class LightSphere {
    double x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    double y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

    double z(float r, float phi, float theta){
        return r*cos(phi);
    }

public:
    std::vector<float> coords;
    LightSphere(unsigned int n, float radius, float r, float g, float b) {
        int n_steps = (n%2==0) ? n : n+1;
        double step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps; ++i) {
            for (int j = 0; j < n_steps/2.0; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n_steps)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                double vij_x = x(radius, phi_i, theta_j);
                double vij_y = y(radius, phi_i, theta_j);
                double vij_z = z(radius, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(radius, phi_ip1, theta_j);
                double vip1j_y = y(radius, phi_ip1, theta_j);
                double vip1j_z = z(radius, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(radius, phi_i, theta_jp1);
                double vijp1_y = y(radius, phi_i, theta_jp1);
                double vijp1_z = z(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                double vip1jp1_y = y(radius, phi_ip1, theta_jp1);
                double vip1jp1_z = z(radius, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }
};

class Torus {
    double x(float c, float a, float phi, float theta){
        return (c+a*cos(theta))*cos(phi);
    }

    double y(float c, float a, float phi, float theta){
        return (c+a*cos(theta))*sin(phi);
    }

    double z(float c, float a, float phi, float theta){
        return a*sin(theta);
    }

public:
    std::vector<float> coords;
    Torus(unsigned int n, float c, float a, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n)*step_size;

                // vertex i,j
                double vij_x = x(c, a, phi_i, theta_j);
                double vij_y = y(c, a, phi_i, theta_j);
                double vij_z = z(c, a, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(c, a, phi_ip1, theta_j);
                double vip1j_y = y(c, a, phi_ip1, theta_j);
                double vip1j_z = z(c, a, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(c, a, phi_i, theta_jp1);
                double vijp1_y = y(c, a, phi_i, theta_jp1);
                double vijp1_z = z(c, a, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(c, a, phi_ip1, theta_jp1);
                double vip1jp1_y = y(c, a, phi_ip1, theta_jp1);
                double vip1jp1_z = z(c, a, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }

};

#endif
