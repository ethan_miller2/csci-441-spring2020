#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix4.h>
#include <csci441/vector4.h>
#include <csci441/uniform.h>

#include "shape.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

int shape = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}


Matrix4 processModel(const Matrix4& model, GLFWwindow *window) {
    Matrix4 trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

Matrix3 invertTrans(const Matrix4& model){
    Matrix3 normModel;

    normModel.inverse_transpose(model);

    return normModel;
}

void processInput(Matrix4& model, Matrix3& normModel, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    else if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS){
        shape++;
        glfwWaitEventsTimeout(0.7);
    }

    model = processModel(model, window);
    normModel = invertTrans(model);

}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "lab5", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }


    // create c
    Cylinder c3(20, 1, .2, .4);
    Cone c2(1000, 1, .2, .4);
    Sphere c4(100, .5, 1, .2, .4);
    LightSphere c1(20, .5, 1, .2, .4);
    Torus c5(20, .75, .25, 1, .2, .4);
    DiscoCube c;
    
    // copy vertex data
    GLuint VBO[6];
    GLuint VAO[6];



    //Vertex buffer and array for discocube
    glGenBuffers(1, &VBO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, c.coords.size()*sizeof(float),
            &c.coords[0], GL_STATIC_DRAW);
  

    glGenVertexArrays(1, &VAO[0]);
    glBindVertexArray(VAO[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);

//====================================================================== 

    //Vertex buffer and array for lightsphere
    glGenBuffers(1, &VBO[1]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, c1.coords.size()*sizeof(float),
            &c1.coords[0], GL_STATIC_DRAW);

    glGenVertexArrays(1, &VAO[1]);
    glBindVertexArray(VAO[1]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

//====================================================================== 
    //Vertex buffer and array for cone
    glGenBuffers(1, &VBO[2]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[2]);
    glBufferData(GL_ARRAY_BUFFER, c2.coords.size()*sizeof(float),
            &c2.coords[0], GL_STATIC_DRAW);
  
    glGenVertexArrays(1, &VAO[2]);
    glBindVertexArray(VAO[2]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);

//====================================================================== 

    //Vertex buffer and array for cylinder
    glGenBuffers(1, &VBO[3]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[3]);
    glBufferData(GL_ARRAY_BUFFER, c3.coords.size()*sizeof(float),
            &c3.coords[0], GL_STATIC_DRAW);
  

    glGenVertexArrays(1, &VAO[3]);
    glBindVertexArray(VAO[3]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);


//====================================================================== 

    glGenBuffers(1, &VBO[4]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[4]);
    glBufferData(GL_ARRAY_BUFFER, c4.coords.size()*sizeof(float),
            &c4.coords[0], GL_STATIC_DRAW);
  

    glGenVertexArrays(1, &VAO[4]);
    glBindVertexArray(VAO[4]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);


     glGenBuffers(1, &VBO[5]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[5]);
    glBufferData(GL_ARRAY_BUFFER, c5.coords.size()*sizeof(float),
            &c5.coords[0], GL_STATIC_DRAW);
  

    glGenVertexArrays(1, &VAO[5]);
    glBindVertexArray(VAO[5]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));




    // setup projection
    Matrix4 projection;
    projection.perspective(45, 1, .01, 10);





    // setup view
    Vector4 eye(0, 0, 3);
    Vector4 origin(0, 0, 0);
    Vector4 up(0, 1, 0);
    float lightX = 1.2f;
    float lightY = 1.0f;
    float lightZ = 2.0f;
    Vector4 lightPos(lightX, lightY, lightZ);

   
    Vector4 lightColor(1.0f, 1.0f, 1.0f);

    Matrix4 camera;
    camera.look_at(eye, origin, up);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");
    Shader lightShader("../lightVert.glsl", "../lightFrag.glsl");

    // setup the textures
    shader.use();
    lightShader.use();

    // set the matrices
    Matrix4 model;
    Matrix4 lightModel;


    Matrix3 normModel;



    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {

        // process input
        processInput(model, normModel, window);



        float lightX = 2.0f * sin(glfwGetTime());
        float lightY = -0.3f;
        float lightZ = 1.5f * cos(glfwGetTime());
        Vector4 lightPos(lightX,lightY,lightZ);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        Uniform::set(shader.id(), "model", model);
        Uniform::set(shader.id(), "projection", projection);
        Uniform::set(shader.id(), "camera", camera);
        Uniform::set(shader.id(), "normModel", normModel);
        Uniform::set(shader.id(), "lightPos", lightPos);
        Uniform::set(shader.id(), "lightColor", lightColor);
        Uniform::set(shader.id(), "viewPos", eye);


        if(shape % 5 == 0){
            glBindVertexArray(VAO[0]);
            glDrawArrays(GL_TRIANGLES, 0, c.coords.size()*sizeof(float));
        }else if(shape % 5 == 1){
    
            glBindVertexArray(VAO[2]);
            glDrawArrays(GL_TRIANGLES, 0, c2.coords.size()*sizeof(float));
        }
        else if(shape % 5 == 2){
            glBindVertexArray(VAO[3]);
            glDrawArrays(GL_TRIANGLES, 0, c3.coords.size()*sizeof(float));

        }
        else if(shape % 5 == 3){
            glBindVertexArray(VAO[4]);
            glDrawArrays(GL_TRIANGLES, 0, c4.coords.size()*sizeof(float));

        }
        else{
            glBindVertexArray(VAO[5]);
            glDrawArrays(GL_TRIANGLES, 0, c5.coords.size()*sizeof(float));

        }
        

        lightShader.use();

        lightModel.translate(lightX,lightY,lightZ);


        Uniform::set(lightShader.id(), "model", lightModel);
        Uniform::set(lightShader.id(), "projection", projection);
        Uniform::set(lightShader.id(), "camera", camera);


        glBindVertexArray(VAO[1]);
        glDrawArrays(GL_TRIANGLES, 0, c1.coords.size()*sizeof(float));

        

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
