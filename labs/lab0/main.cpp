#include <iostream>
#include <string>
using namespace std;

class Vector3 {
public:
	float x;
	float y;
	float z;

	Vector3(){
		x=0;
		y=0;
		z=0;
	}

	// Constructor
	Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
		// nothing to do here as we've already initialized x, y, and z above
		 cout << "in Vector3 constructor" << endl;
	}

  

	// Destructor - called when an object goes out of scope or is destroyed
	~Vector3() {
		// this is where you would release resources such as memory or file descriptors
		// in this case we don't need to do anything
		 cout << "in Vector3 destructor" << endl;
	}
};

  Vector3 add(const Vector3& v, const Vector3& v2) { // v and v2 are copies, so any changes to them in this function
									 // won't affect the originals
	// ...
		Vector3 temp(v.x + v2.x, v.y + v2.y, v.z + v2.z);
		return temp;


}

Vector3 operator+(const Vector3& v, const Vector3& v2) {
	Vector3 temp(v.x + v2.x, v.y + v2.y, v.z + v2.z);
		return temp;

}

Vector3 operator*(float s, Vector3 v) {
	Vector3 temp(v.x * s, v.y * s, v.z * s);
		return temp;

}

Vector3 operator*(Vector3 v, float s) {
	Vector3 temp(v.x * s, v.y * s , v.z * s);
		return temp;

}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector
    stream << "Vector: (" << v.x << "," << v.y << "," << v.z << ")" << endl;

    return stream;
}

void printVector(Vector3 v) {
	cout << "Vector: (" << v.x << "," << v.y << "," << v.z << ")" << endl;
}


int main(int argc, char** argv) {\
	char name[100];



	cout << "hello world " << argv[0] << " " << 1234 << " " << endl;
	cout << "COMMAND LINE TOOLS" <<  endl;
	cout << "Enter Your name " << endl;

	cin >> name;

	cout << "Hello " << name << endl;

	cout << "***********************" << endl;

	Vector3 a(1,2,3);   // allocated to the stack
    Vector3 b(4,5,6);

    Vector3 c = add(a,b); // copies 6 floats to the arguments in add (3 per Vector3),
                          // 3 more floats copied into c when it returns
                          // a and b are unchanged
    cout << "VECTOR ADDITION" <<  endl;
    printVector(a);
    
    printVector(b);
    cout << "----------------" << endl;
    printVector(c);

    cout << "***********************" << endl;
    cout << "OPERATOR OVERLOADING" << endl;

    Vector3 v(1,2,3);
	Vector3 v2(4,5,6);

	cout << v+v2 << endl;

	cout << "***********************" << endl;
	Vector3 vec(0,5,0);
	vec.y = 5; 
	cout << vec << endl;

	cout << "***********************" << endl;
	cout << "POINTERS" << endl;

	Vector3* vect = new Vector3(0,0,0);
	vect->y = 5;

	cout << *vect << endl;

	delete vect; 

	cout << "***********************" << endl;
	cout << "ARRAYS" << endl;

	Vector3 arrayOfVectors[10];
	Vector3* ptr = arrayOfVectors;

	Vector3* heapArrayOfVectors= new Vector3[10];

	for(int i = 0; i < 10; i++)
	{
		heapArrayOfVectors[i].y = 5;
		cout << heapArrayOfVectors[i] << endl;
	}

	delete [] heapArrayOfVectors;

	cout << "***********************" << endl;
	cout << "REFERENCES" << endl;






}