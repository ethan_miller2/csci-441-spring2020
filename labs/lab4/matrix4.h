
#ifndef _CSCI441_MATRIX4_H_
#define _CSCI441_MATRIX4_H_
using namespace std;
#include <string>
#include <sstream>
#define _USE_MATH_DEFINES // for C++
#include <cmath>
#include "vector3.h"


class Matrix4 {
	float values[16];
	
	
public:
	

	Matrix4() {
	}


	void initToIdentity(){
		for(int i = 0; i < 16; i++){
			if(i % 5 == 0){
				values[i] = 1;
			}
			else{
				values[i] = 0;
			}
		}
	}

	void initToZero(){
		for(int i = 0; i < 16; i++){
			values[i] = 0;
		}
	}


	void initToOrthographic(){
		initToZero();
		initToIdentity();
		float near = -1;
		float far = 1;
		float right = 1;
		float left = -1;
		float top = 1;
		float bottom = -1; 
		values[0] = 2 / (right - left);
		values[3] = -((right + left) / (right - left));
		values[5] = 2 / (top - bottom);
		values[7] = - ((top + bottom) / (top - bottom));
		values[10] = (-2) / (far - near);
		values[11] = - ((far + near) / (far - near));
	}

	void initToPerspective(){
		initToZero();
		float near = .1;
		float far = 100;
		float right = 1;
		float left = -1;
		float top = 1;
		float bottom = -1; 
		values[0] = ((2 * near) / (right - left));
		values[2] = ((right + left) / (right - left));
		values[5] = ((2 * near) / (top - bottom));
		values[6] = (top + bottom) / (top - bottom);
		values[10] = ((-(far + near)) / (far - near));
		values[11] = ((-2 * far * near) / (far - near));
		values[14] = -1;
	}

	void initToView(float wx,  float wy){
		initToZero();
		initToIdentity();
		values[0] = wx / 2;
		values[3] = (wx - 1) / 2;
		values[5] = wy / 2;
		values[7] = (wy - 1) / 2;
	}

	void initToViewTranslate(float y){
		initToZero();
		initToIdentity();
		values[7] = -y;
	}

	void initToRotationX(float x){
		float theta = x * M_PI/180;

		initToZero();
		initToIdentity();
		values[5] = cos(theta);
		values[6] = -sin(theta);
		values[9] = sin(theta);
		values[10] = cos(theta);
	 
	}

	void initToRotationY(float x){
		float theta = x * M_PI/180;

		initToZero();
		initToIdentity();
		values[0] = cos(theta);
		values[2] = sin(theta);
		values[8] = -sin(theta);
		values[10] = cos(theta);
	}

	void initToTranslation(float x, float y, float z){
		initToZero();
		initToIdentity();
		values[3] = x;
		values[7] = y;
		values[11] = z; 
	}

	void initToRotationZ(float x){
		float theta = x * M_PI/180;

		initToZero();
		initToIdentity();
		values[0] = cos(theta);
		values[1] = -sin(theta);
		values[4] = sin(theta);
		values[5] = cos(theta);
	}
	
	void initToScale(float x, float y, float z){
		initToZero();
		initToIdentity();
		values[0] = x;
		values[5] = y;
		values[10] = z;
	}

	void initToLook(float yNum){

		initToZero();
		initToIdentity();


		static float* z; 
		static float* x;
		static float y[3];

		Vector3* eye = new Vector3(0.0, yNum, -20.0);
		Vector3* target = new Vector3(0.0, 0.0, 0.0);
		Vector3* up = new Vector3(0.0, 1.0, 0.0);

		z = normalize(minus(eye->getValues(), target->getValues()));



		x = normalize(multiplyVec(up->getValues(),z));



		multiplyVec(z, x, y);

		values[0] = x[0];
		values[1] = x[1];
		values[2] = x[2];
		values[4] = y[0];
		values[5] = y[1];
		values[6] = y[2];
		values[8] = z[0];
		values[9] = z[1];
		values[10] = z[2];

		to_string();
	}

	void initToCameraMove(float y){
		initToZero();
		initToIdentity();
		values[7] = -y;
	}

	float* multiplyVec(float a[], float b[]){
		static float c[3];

		c[0] = (a[1] * b[2]) - (a[2] * b[1]);
		c[1] = (a[2] * b[0]) - (a[0] * b[2]);
		c[2] = (a[0] * b[1]) - (a[1] * b[0]);


		return c;
	}
	void multiplyVec(float a[], float b[], float c[]){
		c[0] = (a[1] * b[2]) - (a[2] * b[1]);
		c[1] = (a[2] * b[0]) - (a[0] * b[2]);
		c[2] = (a[0] * b[1]) - (a[1] * b[0]);
	}
	
	float* minus(float a[], float b[]){
		static float x[3];

		x[0] = a[0] - b[0];
		x[1] = a[1] - b[1];
		x[2] = a[2] - b[2];


		return x;
	}

	float* normalize(float a[]){
		float magnitude = sqrt(pow(a[0], 2) + pow(a[1], 2) + pow(a[2], 2));
		a[0] = a[0] / magnitude;
		a[1] = a[1] / magnitude;
		a[2] = a[2] / magnitude;
		return a;
	}

	
	float* getValues(){
		return values;
	}



	string to_string() const{
		ostringstream os;
		for(int i = 0; i < 4; i++){
			os << values[i] << " ";

		}
		os << endl;
		for(int i = 4; i < 8; i++){
			os << values[i] << " ";

		}
		os << endl;
		for(int i = 8; i < 12; i++){
			os << values[i] << " ";

		}
		os << endl;
		for(int i = 12; i < 16; i++){
			os << values[i] << " ";

		}
		os << endl;
		

		return os.str();	
	}


};


#endif