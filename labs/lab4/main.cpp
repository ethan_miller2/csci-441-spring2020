#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#define _USE_MATH_DEFINES // for C++
#include <cmath>
using namespace std;

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "matrix4.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
float xDegrees = 0.0f;
float yDegrees = 0.0f;
float zDegrees = 0.0f;
float scaleX = 1;
float scaleY = 1;
float scaleZ = 1;
float transX;
float transY;
float transZ;
float yNum;
float * ptr;
int mode = 0;
int shape = 0;
float pointerMode;
float * ptrCam;
float * ptrProjection;


float aspectRatio = SCREEN_WIDTH / SCREEN_HEIGHT;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
// adjust accordingly
    const float rotationIncrement = 1.0f;
    const float scaleIncrement = 0.01f;
    const float transIncrement = 0.01;
    const float cameraIncrement = 0.01;
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS){
        xDegrees -= rotationIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS){
        xDegrees += rotationIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS){
        yDegrees -= rotationIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS){
        yDegrees += rotationIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS){
        zDegrees -= rotationIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS){
        zDegrees += rotationIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS){
        scaleX -= scaleIncrement;
        scaleY -= scaleIncrement;
        scaleZ -= scaleIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS){
        scaleX += scaleIncrement;
        scaleY += scaleIncrement; 
        scaleZ += scaleIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
        transX += transIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
        transX -= transIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        transY -= transIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
        transY += transIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS){
        transZ -= transIncrement;
    }
    if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS){
        transZ += transIncrement;
    }
    if(yNum > -20){
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
            yNum -= cameraIncrement;
        }   
    }
    if(yNum < 20){
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
            yNum += cameraIncrement;
        }
    }
    if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_PRESS){
        mode++;
        if(pointerMode == 0){
            transZ += 1.2;
            pointerMode = 1;
        }else{
            transZ -= 1.2;
            pointerMode = 0;
        }
        
        glfwWaitEventsTimeout(0.7);
    }
    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS){
        cout << shape;
        shape++;
        glfwWaitEventsTimeout(0.7);
    }

}

float* multiply(float a[], float b[]){
    float * result;
    result = new float [16];
    result[0] = (a[0] * b[0]) + (a[1] * b[4]) + (a[2] * b[8]) + (a[3] * b[12]);
    result[1] = (a[0] * b[1]) + (a[1] * b[5]) + (a[2] * b[9]) + (a[3] * b[13]);
    result[2] = (a[0] * b[2]) + (a[1] * b[6]) + (a[2] * b[10]) + (a[3] * b[14]);
    result[3] = (a[0] * b[3]) + (a[1] * b[7]) + (a[2] * b[11]) + (a[3] * b[15]);

    result[4] = (a[4] * b[0]) + (a[5] * b[4]) + (a[6] * b[8]) + (a[7] * b[12]);
    result[5] = (a[4] * b[1]) + (a[5] * b[5]) + (a[6] * b[9]) + (a[7] * b[13]);
    result[6] = (a[4] * b[2]) + (a[5] * b[6]) + (a[6] * b[10]) + (a[7] * b[14]);
    result[7] = (a[4] * b[3]) + (a[5] * b[7]) + (a[6] * b[11]) + (a[7] * b[15]);

    result[8] = (a[8] * b[0]) + (a[9] * b[4]) + (a[10] * b[8]) + (a[11] * b[12]);
    result[9] = (a[8] * b[1]) + (a[9] * b[5]) + (a[10] * b[9]) + (a[11] * b[13]);
    result[10] = (a[8] * b[2]) + (a[9] * b[6]) + (a[10] * b[10]) + (a[11] * b[14]);
    result[11] = (a[8] * b[3]) + (a[9] * b[7]) + (a[10] * b[11]) + (a[11] * b[15]);

    result[12] = (a[12] * b[0]) + (a[13] * b[4]) + (a[14] * b[8]) + (a[15] * b[12]);
    result[13] = (a[12] * b[1]) + (a[13] * b[5]) + (a[14] * b[9]) + (a[15] * b[13]);
    result[14] = (a[12] * b[2]) + (a[13] * b[6]) + (a[14] * b[10]) + (a[15] * b[14]);
    result[15] = (a[12] * b[3]) + (a[13] * b[7]) + (a[14] * b[11]) + (a[15] * b[15]);

    return result;
}

float* staticImage(){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToZero();
    matrixPointer->initToIdentity();
    return matrixPointer->getValues();
 }

float * rotateX(float degrees){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToRotationX(degrees);
    return matrixPointer->getValues();
 }

 float * rotateY(float degrees){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToRotationY(degrees);
    return matrixPointer->getValues();
 }

 float * rotateZ(float degrees){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToRotationZ(degrees);
    return matrixPointer->getValues();
 }

 float * scale(float x, float y, float z){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToScale(x,y,z);
    return matrixPointer->getValues();
 }

 float * translate(float x, float y, float z){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToTranslation(x,y,z);
    return matrixPointer->getValues();
 }

 float * look(float y){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToLook(y);
    return matrixPointer->getValues();
 }

 float * viewTranslate(float y){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToCameraMove(y);
    return matrixPointer->getValues();
 }

 float * orthographic(){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToOrthographic();
    return matrixPointer-> getValues();
 }


 float * perspective(){
    Matrix4* matrixPointer = new Matrix4();
    matrixPointer->initToPerspective();
    return matrixPointer->getValues();

 }

void printMatrix(float a[]){
    for (int i = 0; i < 4; i++){
        cout << " " << a[i];
    }
    cout << endl;


    for (int i = 4; i < 8; i++){
        cout << " " << a[i];
    }
    cout << endl;


    for (int i = 8; i < 12; i++){
        cout << " " << a[i];
    }
    cout << endl;


    for (int i = 12; i < 16; i++){
        cout << " " << a[i];
    }
    cout << endl;

}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    float vertices[] = {
        -0.3f, -0.3f, -0.3f,  0.0f, 0.0f, 1.0f,
         0.3f, -0.3f, -0.3f,  0.0f, 0.0f, 1.0f,
         0.3f,  0.3f, -0.3f,  0.0f, 0.0f, 1.0f,
         0.3f,  0.3f, -0.3f,  0.0f, 0.0f, 1.0f,
        -0.3f,  0.3f, -0.3f,  0.0f, 0.0f, 1.0f,
        -0.3f, -0.3f, -0.3f,  0.0f, 0.0f, 1.0f,

        -0.3f, -0.3f,  0.3f,  0.0f, 0.0f, 1.0f,
         0.3f, -0.3f,  0.3f,  0.0f, 0.0f, 1.0f,
         0.3f,  0.3f,  0.3f,  0.0f, 0.0f, 1.0f,
         0.3f,  0.3f,  0.3f,  0.0f, 0.0f, 1.0f,
        -0.3f,  0.3f,  0.3f,  0.0f, 0.0f, 1.0f,
        -0.3f, -0.3f,  0.3f,  0.0f, 0.0f, 1.0f,

        -0.3f,  0.3f,  0.3f,  1.0f, 0.0f, 0.0f,
        -0.3f,  0.3f, -0.3f,  1.0f, 0.0f, 0.0f,
        -0.3f, -0.3f, -0.3f,  1.0f, 0.0f, 0.0f,
        -0.3f, -0.3f, -0.3f,  1.0f, 0.0f, 0.0f,
        -0.3f, -0.3f,  0.3f,  1.0f, 0.0f, 0.0f,
        -0.3f,  0.3f,  0.3f,  1.0f, 0.0f, 0.0f,

         0.3f,  0.3f,  0.3f,  1.0f, 0.0f, 0.0f,
         0.3f,  0.3f, -0.3f,  1.0f, 0.0f, 0.0f,
         0.3f, -0.3f, -0.3f,  1.0f, 0.0f, 0.0f,
         0.3f, -0.3f, -0.3f,  1.0f, 0.0f, 0.0f,
         0.3f, -0.3f,  0.3f,  1.0f, 0.0f, 0.0f,
         0.3f,  0.3f,  0.3f,  1.0f, 0.0f, 0.0f,

        -0.3f, -0.3f, -0.3f,  0.0f, 1.0f, 0.0f,
         0.3f, -0.3f, -0.3f,  0.0f, 1.0f, 0.0f,
         0.3f, -0.3f,  0.3f,  0.0f, 1.0f, 0.0f,
         0.3f, -0.3f,  0.3f,  0.0f, 1.0f, 0.0f,
        -0.3f, -0.3f,  0.3f,  0.0f, 1.0f, 0.0f,
        -0.3f, -0.3f, -0.3f,  0.0f, 1.0f, 0.0f,

        -0.3f,  0.3f, -0.3f,  0.0f, 1.0f, 0.0f,
         0.3f,  0.3f, -0.3f,  0.0f, 1.0f, 0.0f,
         0.3f,  0.3f,  0.3f,  0.0f, 1.0f, 0.0f,
         0.3f,  0.3f,  0.3f,  0.0f, 1.0f, 0.0f,
        -0.3f,  0.3f,  0.3f,  0.0f, 1.0f, 0.0f,
        -0.3f,  0.3f, -0.3f,  0.0f, 1.0f, 0.0f
    };

    float vertices2[] = {

        0.0f,  0.3f,  0.0f,   1.0f, 1.0f, 0.0f, 
       -0.3f, -0.3f,  0.3f,   1.0f, 1.0f, 0.0f,
        0.3f, -0.3f,  0.3f,   1.0f, 1.0f, 0.0f,

        0.0f,  0.3f,  0.0f,   0.0f, 1.0f, 1.0f,      
       -0.3f, -0.3f,  0.3f,   0.0f, 1.0f, 1.0f,
        0.0f, -0.3f, -0.3f,   0.0f, 1.0f, 1.0f,

        0.0f,  0.3f,  0.0f,   0.0f, 0.0f, 0.0f,
        0.0f, -0.3f, -0.3f,   0.0f, 0.0f, 0.0f,
        0.3f, -0.3f,  0.3f,   0.0f, 0.0f, 0.0f,

       -0.3f, -0.3f,  0.3f,   1.0f, 1.0f, 1.0f,
        0.0f, -0.3f, -0.3f,   1.0f, 1.0f, 1.0f,
        0.3f, -0.3f,  0.3f,   1.0f, 1.0f, 1.0f
    };

    // copy vertex data
    GLuint VBO[2];
    glGenBuffers(2, &VBO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

    GLuint VAO[2];

    glGenVertexArrays(2, &VAO[0]);
    glBindVertexArray(VAO[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_DYNAMIC_DRAW);

    glBindVertexArray(VAO[1]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);


    // GLuint VBO[1];
    // glGenBuffers(1, VBO);
    // glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    // glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);

    // // create and bind the vertex array object and describe data layout
    // GLuint VAO[1];
    // glGenVertexArrays(1, VAO);
    // glBindVertexArray(VAO[0]);

    // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(0*sizeof(float)));
    // glEnableVertexAttribArray(0);

    // glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 6*sizeof(float), (void*)(3*sizeof(float)));
    // glEnableVertexAttribArray(1);


    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

         ptr = multiply(rotateX(xDegrees),rotateY(yDegrees));
         ptr = multiply(ptr,rotateZ(zDegrees));
         ptr = multiply(ptr, translate(transX,transY,transZ));
         ptr = multiply(ptr, scale(scaleX,scaleY,scaleZ));


        unsigned int transformLoc = glGetUniformLocation(shader.id(), "transform");
        glUniformMatrix4fv(transformLoc, 1, GL_TRUE, ptr);

        ptrCam = multiply(look(yNum),viewTranslate(yNum));


        unsigned int viewLoc = glGetUniformLocation(shader.id(), "view");
        glUniformMatrix4fv(viewLoc, 1, GL_TRUE, ptrCam);

        if(mode % 2 == 0){
            ptrProjection = orthographic();
        }
        else{
            ptrProjection = perspective();
        }

        unsigned int projectionLoc = glGetUniformLocation(shader.id(),"projection");
        glUniformMatrix4fv(projectionLoc,1,GL_TRUE,ptrProjection);

        if(shape % 2 == 0){
            glBindVertexArray(VAO[0]);
        }else{
            glBindVertexArray(VAO[1]);
        }
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));
        

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
