
#ifndef _CSCI441_VECTOR3_H_
#define _CSCI441_VECTOR3_H_
using namespace std;

#include <string>
#include <sstream>
#define _USE_MATH_DEFINES // for C++
#include <cmath>


class Vector3{
	float values[3];
	
	
public:
	

	Vector3(float x, float y, float z) {
		values[0] = x;
		values[1] = y;
		values[2] = z;
	}


	float* getValues(){
		return values;
	}


};
#endif